---
layout: job_page
title: "Head of Product"
---

The Head of Product reports to the CEO and is an individual contributor.

## Responsibilities

- Lead product on continuous integration and delivery / deployment
- Create an attractive roadmap (direction) for CI/CD
- Work together with the VP of Product and other stakeholders ensuring that
  CI/CD is a deeply integrated part of GitLab
- Handle feature proposals from customers and the rest of the community related to CI/CD
- Work together with CI Lead on delivering a stable CI/CD platform
- Make sure that CI/CD is well documented, with examples for most common use cases and programming languages
- Work together with marketing (senior product marketing manager) to show off our CI/CD offering on our website
- Ensure the success of CI/CD by filling in any gaps such as writing product announcements and blog posts
