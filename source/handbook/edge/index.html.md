---
layout: markdown_page
title: "Edge Team"
---

The Edge Team is part of engineering responsible for improving GitLab
in multiple ways:

  * Improving GitLab code/workflows in ways that backend teams may not be doing
    * [Backstage](/jobs/specialist/backstage/) improvements
    * Identifying architectural improvements
  * Interfacing with the community
    * [Triaging issues](/jobs/specialist/issue-triage/) reported by the community
    * [Reviewing and accepting merge requests](/jobs/merge-request-coach/) submitted by the community
  * Advancing GitLab's contributions to other related open source projects
    * For example: git command-line, Gogs
  * Evaluating code quality in potential partnerships
